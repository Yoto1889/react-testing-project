import md5 from 'md5';
import { v4 as uuidv4 } from 'uuid';
import jwt from 'jsonwebtoken';
import Roles from '../constants/roles';
import initialUsers from './user-data.json';
import ROLES from '../constants/roles';

const secret = "sphere-secret";

let users = [...initialUsers];

function hasPermission(user, authUser) {
  if (authUser.role === Roles.ADMIN) {
    return true;
  }
  if (authUser.id === authUser.id) {
    return true;
  }

  if (authUser.role === Roles.CLIENT && authUser.id === user.clientId) {
    return true;
  }

  return false;
}
async function login({ email, password }) {
  const hashedPassword = md5(password);
  const user = users.find(user => user.email === email && user.password === hashedPassword);
  if (!user) {
    throw new Error("Can't find user with the credentials.");
  }

  const newUser = { ...user };
  delete newUser.password;
  const token = jwt.sign({ username: user.username, role: user.role, id: user.id }, secret);
  return {
    data: {
      token,
      user: newUser,
    },
  };
}

async function signup(data) {
  const { username, email, password } = data;
  const userAlreadyExists = users.some(user => user.email === email || user.username === username);
  if (userAlreadyExists) {
    throw new Error("User with username or email already exists.!");
  }
  const hashedPassword = md5(password);
  const user = {
    ...data,
    id: uuidv4(),
    password: hashedPassword
  };
  const token = jwt.sign({ username: user.username, role: user.role, id: user.id }, secret);
  users.push(user);
  return { user, token };
}

async function getProfile(token) {
  try {
    var { username } = jwt.verify(token, secret);
    const user = users.find(user => user.username === username);
    if (!user) {
      throw new Error("User does not exist!");
    }
    return user;
  } catch (err) {
    throw new Error(err);
  }
}

async function getUsers(params, token) {
  try {
    var { username, role, id } = jwt.verify(token, secret);
    const user = users.find(user => user.username === username);
    if (!user) {
      throw new Error("Invalid Token!");
    }

    if (user.role === Roles.USER) {
      throw new Error("You don't have enough permissions.");
    }

    const { offset, limit } = params;
    return users.filter(user => user.username !== username && (role === ROLES.ADMIN || user.role === ROLES.USER && user.clientId === id)).slice(offset, offset + limit).map(user => {
      const newUser = { ...user };
      delete newUser.password;
      return newUser;
    });
  } catch (err) {
    throw new Error(err);
  }
}

async function updateUser(params, token) {
  try {
    var { username } = jwt.verify(token, secret);
    const authUser = users.find(user => user.username === username);
    if (!authUser) {
      throw new Error("Invalid Token!");
    }

    const userIndex = users.findIndex(user => user.id === params.id);
    if (userIndex < 0) {
      throw new Error("Invalid payload.");
    }

    const user = users[userIndex];
    if (!hasPermission(user, authUser)) {
      throw new Error("Permission Denied");
    }

    const updatedUser = { ...user, ...params };
    if (params.password) {
      updatedUser.password = md5(params.password);
    };
    users[userIndex] = { ...updatedUser };
    delete updatedUser.password;
    return updatedUser;
  } catch (err) {
    throw new Error(err);
  }
}

async function updateProfile(params, token) {
  try {
    var { username } = jwt.verify(token, secret);

    const userIndex = users.findIndex(user => user.username === username);
    if (userIndex < 0) {
      throw new Error("Invalid token.");
    }

    const duplicated = users.some(users => user.email === params.email || user.username === params.password);
    if (duplicated) {
      throw new Error("Username or email already exists!");
    }

    const user = users[userIndex];

    const updatedUser = { ...user, ...params };
    if (params.password) {
      updatedUser.password = md5(params.password);
    };
    users[userIndex] = { ...updatedUser };
    delete updatedUser.password;
    return updatedUser;
  } catch (err) {
    throw new Error(err);
  }
}

async function deleteUser(userId, token) {
  try {
    var { username } = jwt.verify(token, secret);
    const authUser = users.find(user => user.username === username);
    if (!authUser) {
      throw new Error("Invalid Token!");
    }

    const user = users.find(user => user.id === userId);

    if (!hasPermission(user, authUser)) {
      throw new Error("Permission Denied");
    }
    users = users.filter(({ id }) => user.id !== id);

    return user;
  } catch (err) {
    throw new Error(err);
  }
}

async function addUser(params, token) {
  var { username } = jwt.verify(token, secret);
  const authUser = users.find(user => user.username === username);
  if (!authUser) {
    throw new Error("Invalid Token!");
  }
  if (authUser.role === Roles.USER) {
    throw new Error("You don't have enough permission.");
  };
  const userExists = users.some(user => user.username === params.username || user.email === params.email)
  if (userExists) {
    throw new Error("A user with same email or username already exists.");
  };

  const hashedPassword = md5(params.password);
  const clientId = authUser.role === Roles.CLIENT ? authUser.id : null;
  const newUser = {
    id: uuidv4(),
    ...params,
    clientId,
    password: hashedPassword,
    role: authUser.role === Roles.CLIENT ? Roles.USER : params.role,
  };
  users.push(newUser);
  return newUser;
}
export {
  login,
  getProfile,
  getUsers,
  updateUser,
  updateProfile,
  signup,
  deleteUser,
  addUser,
};