import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Typography,
} from '@material-ui/core';
import { useSnackbar } from 'notistack';
import { Formik } from 'formik';

import validationSchema from './schema';
import { updateUser } from 'src/store/actions/user';
import ROLES from 'src/constants/roles';

const ROLE_OPTIONS = [
  { label: 'Admin User', value: ROLES.ADMIN },
  { label: 'Client User', value: ROLES.CLIENT },
  { label: 'Regular User', value: ROLES.USER },
];

const UserDialog = ({ open, onClose }) => {
  const dispatch = useDispatch();
  const { user, users } = useSelector((state) => state.user);
  const authUser = useSelector(state => state.auth.user);
  const snackbar = useSnackbar();
  const isClient = authUser.role === ROLES.CLIENT ;

  const handleSubmit = (values) => {
    const data = {
      username: values.username,
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      phone: values.phone,
      address: values.address,
      role: values.role,
      clientId: values.clientId,
    };
    if (values.password) {
      data.password = values.password;
    }

    dispatch(
      updateUser(
        user.id,
        data,
        () => {
          onClose();
          snackbar.enqueueSnackbar('Update a user successfully', { variant: 'success' });
        },
        () => {
          onClose();
          snackbar.enqueueSnackbar('Update a user failed', { variant: 'error' });
        }
      )
    );
  };

  return (
    <Dialog onClose={onClose} aria-labelledby="user-dialog" open={open}>
      <Formik
        initialValues={{
          username: user.username || '',
          firstName: user.firstName || '',
          lastName: user.lastName || '',
          email: user.email || '',
          phone: user.phone || '',
          role: user.role || '',
          clientId: user.clientId || '',
          address: user.address || '',
          password: '',
          confirmPassword: '',
        }}
        enableReinitialize
        validationSchema={validationSchema(isClient)}
        onSubmit={handleSubmit}
      >
        {({ errors, handleBlur, handleChange, handleSubmit, touched, values }) => (
          <form onSubmit={handleSubmit}>
            <DialogTitle disableTypography>
              <Typography variant="h4">UPDATE A USER</Typography>
            </DialogTitle>
            <DialogContent>
              <TextField
                error={Boolean(touched.username && errors.username)}
                fullWidth
                helperText={touched.username && errors.username}
                label="Username"
                margin="normal"
                name="username"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.username}
                variant="outlined"
              />
              <Box display="flex" justifyContent="space-between">
                <Box width="48%">
                  <TextField
                    error={Boolean(touched.firstName && errors.firstName)}
                    fullWidth
                    helperText={touched.firstName && errors.firstName}
                    label="First name"
                    margin="normal"
                    name="firstName"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.firstName}
                    variant="outlined"
                  />
                </Box>
                <Box width="48%">
                  <TextField
                    error={Boolean(touched.lastName && errors.lastName)}
                    fullWidth
                    helperText={touched.lastName && errors.lastName}
                    label="Last name"
                    margin="normal"
                    name="lastName"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.lastName}
                    variant="outlined"
                  />
                </Box>
              </Box>
              <Box display="flex" justifyContent="space-between">
                <Box width="48%">
                  <TextField
                    error={Boolean(touched.email && errors.email)}
                    fullWidth
                    helperText={touched.email && errors.email}
                    label="Email"
                    margin="normal"
                    name="email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="email"
                    value={values.email}
                    variant="outlined"
                    />
                  </Box>
                  <Box width="48%">
                    <TextField
                      error={Boolean(touched.phone && errors.phone)}
                      fullWidth
                      helperText={touched.phone && errors.phone}
                      label="Phone"
                      margin="normal"
                      name="phone"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="phone"
                      value={values.phone}
                      variant="outlined"
                    />
                </Box>
              </Box>
              <TextField
                error={Boolean(touched.address && errors.address)}
                fullWidth
                helperText={touched.address && errors.address}
                label="Address"
                margin="normal"
                name="address"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.address}
                variant="outlined"
              />
              {!isClient ?
                <>
                  <TextField
                    error={Boolean(touched.role && errors.role)}
                    fullWidth
                    helperText={touched.role && errors.role}
                    label="User Role"
                    margin="normal"
                    name="role"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.role}
                    variant="outlined"
                    select
                    SelectProps={{ native: true }}
                  >
                    {ROLE_OPTIONS.map((role) => (
                      <option key={role.value} value={role.value}>
                        {role.label}
                      </option>
                    ))}
                  </TextField>
                  {values.role === ROLES.USER ? <TextField
                    error={Boolean(touched.clientId && errors.clientId)}
                    fullWidth
                    helperText={touched.clientId && errors.clientId}
                    label="Client"
                    margin="normal"
                    name="clientId"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.clientId}
                    variant="outlined"
                    select
                    SelectProps={{ native: true }}
                  >
                    {users.filter(user => user.role === ROLES.CLIENT).map((user) => (
                      <option key={user.id} value={user.id}>
                        {user.username}
                      </option>
                    ))}
                  </TextField> : null}
                </> : null}
              <Box display="flex" justifyContent="space-between">
                <Box width="48%">
                  <TextField
                    error={Boolean(touched.password && errors.password)}
                    fullWidth
                    helperText={touched.password && errors.password}
                    label="Password"
                    margin="normal"
                    name="password"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="password"
                    value={values.password}
                    variant="outlined"
                  />
                </Box>
                <Box width="48%">
                  <TextField
                    error={Boolean(touched.confirmPassword && errors.confirmPassword)}
                    fullWidth
                    helperText={touched.confirmPassword && errors.confirmPassword}
                    label="Confirm Password"
                    margin="normal"
                    name="confirmPassword"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="password"
                    value={values.confirmPassword}
                    variant="outlined"
                  />
                </Box>
              </Box>
            </DialogContent>
            <DialogActions>
              <Button onClick={onClose}>CANCEL</Button>
              <Button type="submit" variant="contained" color="primary">
                UPDATE
              </Button>
            </DialogActions>
          </form>
        )}
      </Formik>
    </Dialog>
  );
};

export default UserDialog;
