import * as Yup from 'yup';

function equalTo(ref, msg) {
  return Yup.mixed().test({
    name: 'equalTo',
    exclusive: false,
    message: msg,
    params: {
      reference: ref.path,
    },
    test: function (value) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, 'equalTo', equalTo);

export default (isClient) => (
  Yup.object().shape({
    username: Yup.string().max(255).required('Username is required'),
    email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
    firstName: Yup.string().max(255).required('First name is required'),
    lastName: Yup.string().max(255).required('Last name is required'),
    address: Yup.string().max(255).required('Address is required'),
    confirmPassword: Yup.string().equalTo(Yup.ref('password'), 'Password must be matched'),
    role: !isClient ? Yup.number().required('Role is required') : null,
    clientId: !isClient ? Yup.number().required('Client is required') : null,
  })
);
