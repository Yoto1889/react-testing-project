import React, { useState, useEffect, useCallback } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Layout from '../components/Layout';
import Loader from '../components/Loader';
import LoginPage from '../pages/auth/Login';
import SignupPage from '../pages/auth/Signup';
import HomePage from '../pages/main/Home';
import Settings from '../pages/settings/Settings';
import UsersList from '../pages/user/UsersList';
import NewUser from '../pages/user/NewUser';
import AdminRoute from './AdminRoute';
import ROLES from 'src/constants/roles';
import { getProfile } from 'src/store/actions/auth';

function Routes() {
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => !!state.auth.user);
  const profile = useSelector((state) => state.auth.user);

  const getAccount = useCallback(async () => {
    setLoading(true);
    await dispatch(getProfile());
    setLoading(false);
  }, [dispatch]);

  useEffect(() => {
    if (!!localStorage.getItem('token')) {
      getAccount();
    }
  }, [getAccount]);

  return loading ? (
    <Loader />
  ) : (
    <Switch>
      <Route exact path="/login" component={LoginPage} />
      <Route exact path="/signup" component={SignupPage} />
      {isLoggedIn && (
        <Layout>
          <Switch>
            <Route exact path="/home" component={HomePage} />
              {profile.role !== ROLES.USER ? <Route exact path="/users" component={UsersList} /> : null}
              {profile.role !== ROLES.USER ? <Route exact path="/users/new" component={NewUser} /> : null}
            <Route exact path="/settings" component={Settings} />
          </Switch>
        </Layout>
        )}
        <Route
          path="/"
          render={() => {
            if (isLoggedIn) return <Redirect to="/home" />;
            return <Redirect to="/login" />;
          }}
        />
    </Switch>
  );
}

export default Routes;
