import * as server from 'src/server/server';

class AuthService {
  getToken = () => localStorage.getItem('token');
  login = async (payload) => {
    try {
      const response = await server.login(payload);
      console.log("token", response.data);
      localStorage.setItem('token', response.data);
      return response.data;
    } catch (error) {
      throw error;
    }
  };

  signup = async (payload) => {
    try {
      const response = await server.signup(payload);
      return response;
    } catch (error) {
      throw error;
    }
  };

  getProfile = async () => {
    try {
      const token = localStorage.getItem('token');
      const user = await server.getProfile(token);
      return user;
    } catch (error) {
      throw error;
    }
  };

  updateProfile = async (payload) => {
    try {
      return server.updateProfile(payload, this.getToken());
    } catch (error) {
      throw error;
    }
  };
}

const authService = new AuthService();

export default authService;
