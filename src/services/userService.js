import * as server from '../server/server';

class UserService {
  getToken = () => localStorage.getItem("token");
  getUsers = async (offset, limit) => {
    try {
      const users = await server.getUsers({
        offset,
        limit
      }, this.getToken());
      return { users, totalCount: users.length };
    } catch (error) {
      throw error;
    }
  };

  addUser = async (data) => {
    try {
      const newUser = await server.addUser(data, this.getToken());
      return newUser;
    } catch (error) {
      throw error;
    }
  };

  updateUser = async (userId, data) => {
    try {
      const user = await server.updateUser({
        ...data,
        id: userId,
      }, this.getToken());
      return user;
    } catch (error) {
      throw error;
    }
  };

  deleteUser = async (userId) => {
    try {
      const response = await server.deleteUser(userId, this.getToken());
      return response;
    } catch (error) {
      throw error;
    }
  };
}

const userService = new UserService();

export default userService;
