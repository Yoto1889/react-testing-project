const ROLES = {
  ADMIN: 0,
  CLIENT: 1,
  USER: 2,
};

export default ROLES;
