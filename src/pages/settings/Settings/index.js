import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import {
  Box,
  Button,
  Container,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from '@material-ui/core';
import { useSnackbar } from 'notistack';

import { updateProfile } from 'src/store/actions/auth';

import validationSchema from './schema';

const SettingsPage = () => {
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.auth.user);

  const snackbar = useSnackbar();

  const handleSubmit = (values) => {
    const data = {
      username: values.username,
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      phone: values.phone,
      role: values.role,
    };
    if (values.password) {
      data.password = values.password;
    }
    dispatch(
      updateProfile(
        data,
        () => snackbar.enqueueSnackbar('Update an account successfully', { variant: 'success' }),
        () => snackbar.enqueueSnackbar('Update an account failed', { variant: 'error ' })
      )
    );
  };

  return (
    <Container maxWidth="lg">
        <Grid>
          <Formik
            initialValues={{
              username: profile.username,
              email: profile.email,
              phone: profile.phone,
              firstName: profile.firstName,
              lastName: profile.lastName,
              password: '',
              confirmPassword: '',
            }}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {({ errors, handleBlur, handleChange, handleSubmit, touched, values }) => (
              <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                <Card>
                  <CardHeader subheader="The information can be edited" title="Profile" />
                  <Divider />
                  <CardContent>
                    <TextField
                      error={Boolean(touched.username && errors.username)}
                      fullWidth
                      helperText={touched.username && errors.username}
                      label="Username"
                      margin="normal"
                      name="username"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="username"
                      value={values.username}
                      variant="outlined"
                    />
                    <Box display="flex" justifyContent="space-between">
                      <Box width="48%">
                        <TextField
                          error={Boolean(touched.firstName && errors.firstName)}
                          fullWidth
                          helperText={touched.firstName && errors.firstName}
                          label="First name"
                          margin="normal"
                          name="firstName"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          value={values.firstName}
                          variant="outlined"
                        />
                      </Box>
                      <Box width="48%">
                        <TextField
                          error={Boolean(touched.lastName && errors.lastName)}
                          fullWidth
                          helperText={touched.lastName && errors.lastName}
                          label="Last name"
                          margin="normal"
                          name="lastName"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          value={values.lastName}
                          variant="outlined"
                        />
                      </Box>
                    </Box>
                    <Box display="flex" justifyContent="space-between">
                      <Box width="48%">
                        <TextField
                          error={Boolean(touched.email && errors.email)}
                          fullWidth
                          helperText={touched.email && errors.email}
                          label="Email"
                          margin="normal"
                          name="email"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type="email"
                          value={values.email}
                          variant="outlined"
                        />
                      </Box>
                      <Box width="48%">
                        <TextField
                          error={Boolean(touched.phone && errors.phone)}
                          fullWidth
                          helperText={touched.phone && errors.phone}
                          label="phone"
                          margin="normal"
                          name="Phone Number"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type="phone"
                          value={values.phone}
                          variant="outlined"
                        />
                      </Box>
                    </Box>
                    <Box display="flex" justifyContent="space-between">
                      <Box width="48%">
                        <TextField
                          error={Boolean(touched.password && errors.password)}
                          fullWidth
                          helperText={touched.password && errors.password}
                          label="Password"
                          margin="normal"
                          name="password"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type="password"
                          value={values.password}
                          variant="outlined"
                        />
                      </Box>
                      <Box width="48%">
                        <TextField
                          error={Boolean(touched.confirmPassword && errors.confirmPassword)}
                          fullWidth
                          helperText={touched.confirmPassword && errors.confirmPassword}
                          label="Confirm Password"
                          margin="normal"
                          name="confirmPassword"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type="password"
                          value={values.confirmPassword}
                          variant="outlined"
                        />
                      </Box>
                    </Box>
                  </CardContent>
                  <Divider />
                  <Box display="flex" justifyContent="flex-end" p={2}>
                    <Button type="submit" color="primary" variant="contained" disableElevation>
                      Update Profile
                    </Button>
                  </Box>
                </Card>
              </form>
            )}
          </Formik>
        </Grid>
    </Container>
  );
};

export default SettingsPage;
